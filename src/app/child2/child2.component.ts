import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {WebsocketService} from '../websocket.service';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.css']
})
export class Child2Component implements OnInit {
  private socketSubscription: Subscription;
  url = 'wss://www.tecnosimserver1.com:9008/websocket?token=M0 eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOiIxIiwibWljcm9JZCI6IkNUQSIsImV4cCI6MTU3MTYzMDM5OX0.1qWnjzt-xW1lKHhz3iED09UrSwp1XCvSaZ88KKNShoXwpJUqd7DyX2zGusWBrosjHcNUiMQpae-4MrQTtczEgg';

  constructor(private websocketService: WebsocketService) { }

  ngOnInit() {
    this.init();
  }
  init() {
    this.socketSubscription = this.websocketService.getMiners$(this.url)
      .subscribe((latestStatus: any) => {
        console.log('++++++child1>' + latestStatus);
      }, err => {
        console.log('No more data child2');
        console.error(err);
      });
  }

  close() {
    this.socketSubscription.unsubscribe();
  }
}
