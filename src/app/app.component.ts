import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {WebsocketService} from './websocket.service';
import { retryWhen, tap, delay } from 'rxjs/operators';
import {ConnectionService} from 'ng-connection-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title = 'websocketapp';

  private serverData;
  private conectionStatus = false;
  private socketSubscription: Subscription;

  status = 'ONLINE';
  isConnected = true;
  url = 'wss://www.tecnosimserver1.com:9008/websocket?token=M0 eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOiIxIiwibWljcm9JZCI6IkNUQSIsImV4cCI6MTU3MTYzMDM5OX0.1qWnjzt-xW1lKHhz3iED09UrSwp1XCvSaZ88KKNShoXwpJUqd7DyX2zGusWBrosjHcNUiMQpae-4MrQTtczEgg';

  constructor(private websocketService: WebsocketService, private connectionService: ConnectionService) {
    this.validateInternetConnection();
  }

  ngOnInit(): void {
    // this.init();
  }

  init() {
    const delayTime = 6000;
    this.socketSubscription = this.websocketService.getMiners$(this.url)
      /*.pipe(
        retryWhen(errors =>
          errors.pipe(
            tap(err => {
              console.error('Got error', err);
            }),
            delay(delayTime),
          )
        )
      )*/
      .subscribe((latestStatus: any) => {
        console.log('++++++>' + latestStatus);
        this.serverData = latestStatus;
      }, err => {
        console.log('No more data');
        console.error(err);
      });
  }

  stop() {
    this.websocketService.testNext();
  }

  private validateInternetConnection() {
    this.connectionService.monitor().subscribe(isConnected => {
      console.log('::::::::::::::::');
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = 'ONLINE';
      } else {
        this.status = 'OFFLINE';
        this.websocketService.close(this.url);
      }
      console.log('Internet conexion:' + this.status);
    });
  }
}
