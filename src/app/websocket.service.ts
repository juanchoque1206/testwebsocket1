import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private minerSocket;
  private observable;
  private observer;
  constructor() { }

  public getMiners$(uri) {
    this.observable = new Observable(observer => {
      this.observer = observer;
      try {
        this.minerSocket = webSocket(uri);

        this.minerSocket.subscribe(
          (d) => {
            console.log(':::webSocket Miner arrived');
            observer.next.bind( d );
          },
          (err) => {
            console.warn(':::webSocket Miner DIDNOT arrived');
            observer.error(err);
          },
          () => {
            console.log(':::webSocket COMPLETED');
            this.minerSocket.unsubscribe();
            observer.error('error completed');
            // observer.complete();
          }
        );

        return () => {
          console.log('Observable completed and close!');
          this.minerSocket.unsubscribe();
        };

      } catch (error) {
        observer.error(error);
      }
    });
    return this.observable;
  }

  close(url) {
    try {
      this.minerSocket.unsubscribe();
      // this.observable.getSubject.next('hollaaaa');
      // this.minerSocket.subscribe().next('holllllaa');
      // this.observer.error('error');
      // this.observer.complete();
      this.getMiners$(url);
    } catch (e) {
    }
  }
  closeAll() {
    try {
      this.minerSocket.unsubscribe();
      this.observable.unsubscribe();
    } catch (e) {
    }
  }

  testNext() {
    try {
      this.observer.next('holaaaa');
    } catch (e) {
    }
  }
}
